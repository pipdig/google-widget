<?php
/*
Plugin Name: Google+ Badge Widget
Description: Adds a Google+ Badge as a widget to your site.
Version: 1.0.0
License: GPLv2 or later
Author: Aaron Yarborough
Author URI: https://aaronyarborough.co.uk
Text Domain: aaronjamesy-googleplusbadge-widget
*/

// Block direct requests
if (!defined('ABSPATH')) {
	exit;
}

class AaronJamesY_GooglePlusBadge_Widget extends WP_Widget {
    function __construct() {
        $widget_name = "Google+ Badge";
        $widget_desc = "Display your Google+ Badge.";
        $widget_id = "aaronjamesy_googleplusbadge_widget";

        $widget_ops = array(
            "classname" => $widget_id,
            "description" => $widget_desc
        );

        // Set options for widget
        parent::__construct($widget_id, $widget_name, $widget_ops);
    }

    function widget($args, $instance) {
        echo $args["before_widget"];

        $userid = $instance["userid"];
        $profile_type = $instance["profile_type"];
        $layout = $instance["layout"];
        $width = $instance["width"];
        $color_scheme = $instance["color_scheme"];
        $cover_photo = $instance["cover_photo"];
        $tagline = $instance["tagline"];

		// Choose which kind of profile to load
		switch ($profile_type) {
			case "person":
				$badge_class = "g-person";
				break;
			case "page":
				$badge_class = "g-page";
				break;
		}

		$center = get_option("aaronjamesy_googleplusbadge_widget_center") == "yes" ? true : false;
		$profile_url = sprintf("https://plus.google.com/%s", $userid);

		$html = "";
		$html_load_checker = "<p id='aaronjamesy_googleplusbadge_loadchecker'>Loading Google+ badge...</p>";
		
		$html .= sprintf(
			"<div class='%s' data-href='%s' data-width='%s' data-theme='%s' data-showtagline='%s' data-layout='%s' data-showcoverphoto='%s'>%s</div>",
			$badge_class, $profile_url, $width, $color_scheme, $tagline, $layout, $cover_photo, $html_load_checker
		);

		if ($center) {
			$html = sprintf("<div style='width: %s; text-align: center'>%s</div>", "100%", $html);
		}

		// Print badge to page
		echo $html;

		echo $args["after_widget"];
		?>
			<script type="text/javascript">
				setTimeout(function() {
					var ctrlLoadChecker = document.getElementById("aaronjamesy_googleplusbadge_loadchecker");
					if (ctrlLoadChecker != null) {
						ctrlLoadChecker.style.display = "inline";
						ctrlLoadChecker.innerHTML = "<h4>Couldn't load Google+ Badge</h4><p>An error occured while trying to load your Google+ badge. Please ensure you have configured it correctly!";
					}
				}, 5000);
			</script>
		<?php
    }

    function form($instance) {
		// Defaults
		$userid = "+Pipdig";
		$profile_type = "page";
		$layout = "portrait";
		$width = "180";
		$color_scheme = "light";
		$cover_photo = "enabled";
		$tagline = "enabled";
		$center = "yes";

		// Apply values to editor if they've been set previously
        if (isset($instance)) {
            if (isset($instance["userid"]))			$userid = $instance["userid"];
            if (isset($instance["profile_type"]))		$profile_type = $instance["profile_type"];
            if (isset($instance["layout"]))			$layout = $instance["layout"];
            if (isset($instance["width"]))			$width = $instance["width"];
            if (isset($instance["color_scheme"]))		$color_scheme = $instance["color_scheme"];
            if (isset($instance["cover_photo"]))		$cover_photo = $instance["cover_photo"];
            if (isset($instance["tagline"]))			$tagline = $instance["tagline"];
		  if (isset($instance["center"]))			$center = $instance["center"];
        }
    ?>
        <?php
        // Shows any errors that occured with the previous submission
        if (isset($instance["errors"])) {
            foreach ($instance["errors"] as $error) {
                ?><div style="margin-top: 10px; color: red;">
                    <span><strong>Error: </strong> <?php echo($error); ?></span>
                </div><?php
            }
        }
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('userid'); ?>"><?php _e('Google+ Profile ID (<a href="https://www.plusyourbusiness.com/find-21-digit-id-google-plus-profile-page/" target="_blank">?</a>)', 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('userid'); ?>" name="<?php echo $this->get_field_name('userid'); ?>" type="text" value="<?php echo $userid; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('profile_type'); ?>"><?php _e('Profile Type', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('profile_type'); ?>"
                class="widefat"
                name="<?php echo $this->get_field_name('profile_type'); ?>">
                <option <?php selected($profile_type, "person"); ?> value="person">Person</option>
                <option <?php selected($profile_type, "page"); ?> value="page">Page</option>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('layout'); ?>"><?php _e('Layout', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('layout'); ?>"
                class="widefat"
                name="<?php echo $this->get_field_name('layout'); ?>">
                <option <?php selected($layout, "portrait"); ?> value="portrait">Portrait</option>
                <option <?php selected($layout, "landscape"); ?> value="landscape">Landscape</option>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('width'); ?>"><?php _e("Width", 'wp_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" type="text" value="<?php echo($width); ?>" />
            <label id="<?php echo($this->get_field_id("width") . "_validation"); ?>" style="color: red; display: none;"></label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('color_scheme'); ?>"><?php _e('Color Scheme', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('color_scheme'); ?>"
                class="widefat"
                name="<?php echo $this->get_field_name('color_scheme'); ?>">
                <option <?php selected($color_scheme, "light"); ?> value="light">Light</option>
                <option <?php selected($color_scheme, "dark"); ?> value="dark">Dark</option>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('cover_photo'); ?>"><?php _e('Cover Photo', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('cover_photo'); ?>"
                class="widefat"
                name="<?php echo $this->get_field_name('cover_photo'); ?>">
                <option <?php selected($cover_photo, "true"); ?> value="true">Yes</option>
                <option <?php selected($cover_photo, "false"); ?> value="false">No</option>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('tagline'); ?>"><?php _e('Tagline', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('tagline'); ?>"
                class="widefat"
                name="<?php echo $this->get_field_name('tagline'); ?>">
                <option <?php selected($tagline, "true"); ?> value="true">Yes</option>
                <option <?php selected($tagline, "false"); ?> value="false">No</option>
            </select>
        </p>

		<p>
			<label for="<?php echo $this->get_field_id('center'); ?>"><?php _e('Center in Widget', 'wp_widget_plugin'); ?></label>
            <select id="<?php echo $this->get_field_id('center'); ?>"
                class="widefat"
                name="<?php echo $this->get_field_name('center'); ?>">
                <option <?php selected($center, "yes"); ?> value="yes">Yes</option>
                <option <?php selected($center, "no"); ?> value="no">No</option>
            </select>
		</p>

        <script type="text/javascript">
            // Add validation to width text box
			document.addEventListener("DOMContentLoaded", function() {
				var ctrl = document.getElementById("<?php echo($this->get_field_id("width")) ?>");
	            var ctrlValidation = document.getElementById("<?php echo($this->get_field_id("width")) ?>_validation");
	            ctrl.addEventListener("input", function() {
	                var parsedInt = parseInt(ctrl.value);

	                if (isNaN(parsedInt) || parsedInt < 180 || parsedInt > 450) {
	                    ctrlValidation.style.display = "inline";
	                    ctrlValidation.innerHTML = "Please enter a number between 180 and 450!";
	                } else {
	                    ctrlValidation.style.display = "none";
	                }
	            });
			});
        </script>
    <?php
    }

    function update($new_instance, $old_instance) {
        // Clear previous submission errors
        $old_instance["errors"] = null;

        // Display any needed validation message
        $width_as_int = intval($new_instance["width"]);
        if ($width_as_int < 180 || $width_as_int > 450) {
            $old_instance["errors"][] = "Width must be bigger than 180 but no bigger than 450!";
            return $old_instance;
        }

        return $new_instance;
    }
}

function register_aaronjamesy_googleplusbadge_widget() {
	register_widget( 'AaronJamesY_GooglePlusBadge_Widget' );
}
add_action("widgets_init", "register_aaronjamesy_googleplusbadge_widget");

function aaronjamesy_register_badge_script() { ?>
    <script src='https://apis.google.com/js/platform.js' async defer></script>
    <?php
}
add_action('wp_head', 'aaronjamesy_register_badge_script', 99);

